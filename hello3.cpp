//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    hello3.cpp
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    25_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#include <iostream>

class Cat {
public:
 void sayHello() {
    printf("Meow\n");

 }

};

int main() {

   using namespace std;
Cat myCat;
myCat.sayHello();


}
