//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    hello2.cpp
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    25_FEB_2022
///
////////////////////////////////////////////////////////////////////////////

#include <iostream> 

int main() {

   std::cout << "Hello, world!" << std::endl ;

   return 0;
}
